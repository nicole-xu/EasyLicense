# EasyLicense
An easy license library.

EasyLicense is an open-source license tool for .NET applications. 

---
<div class="row">
  <div class="col-md-6">
    <img class="img-responsive" alt="Screenshot of License Tool" src="https://easyhelper.github.io/EasyLicense/assets/LicenseTool.png" style="height: 300px">
    <small>License Tool</small>
  </div>
  <div class="col-md-6">
    <img class="img-responsive" alt="Screenshot of Demo Project" src="https://easyhelper.github.io/EasyLicense/assets/DemoProject.png" style="height: 300px">
    <small>Demo Project</small>
  </div>
</div>
---

Features
--------
EasyLicense is an open-source license tool for .NET applications.  

<div class="container-fluid">
  <p class="row">
    <ul>
      <li>Generate License Key</li>
      <li>Generate License</li>
      <li>Easy Validate License</li>
    </ul>
  </p>
</div>

---

Downloads
---------
You could obtain the latest source code and releases at [GitHub project page](https://github.com/easyHelper/EasyLicense).
It might be helpful to read the [FAQ](https://easyhelper.github.io/EasyLicense/faq/)!

---

Contribution
------------
EasyLicense is licensed under [MIT license](http://opensource.org/licenses/MIT), 
so you're free to fork and modify it to suit your need!
You could also contribute to the project by creating pull requests and [reporting bugs](https://easyhelper.github.io/EasyLicense/issues/)!

---

Donation
---------
If you find EasyLicense is useful to you, feel free to support the project by making a donation!  

---
<div class="row">
  <div class="col-md-6">
    <img class="img-responsive" alt="Screenshot of License Tool" src="https://easyhelper.github.io/EasyLicense/assets/zhifubao.png" style="height: 300px">
    <small>支付宝</small>
  </div>
  <div class="col-md-6">
    <img class="img-responsive" alt="Screenshot of Demo Project" src="https://easyhelper.github.io/EasyLicense/assets/weixin.png" style="height: 300px">
    <small>微信</small>
  </div>
</div>
---